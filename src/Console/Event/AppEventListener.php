<?php

declare(strict_types=1);

namespace TikiProfilesTester\Console\Event;

interface AppEventListener
{
    /**
     * @param function $dispatcher
     */
    public function register($dispatcher): void;
}
